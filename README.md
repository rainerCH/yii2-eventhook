# yii2-eventhook
Forward yii2 events to different targets (Web, MQTT ...)

## How to use

In your config.php:

```php
<?php
$config = [
    'bootstrap' = ['webhook'],
    'components' = [
        'webhook' => [
            'class' => 'rainerch\eventhook\HttpPost',
            'events' => [
                [['\yii\db\ActiveRecord'], ['afterInsert', 'afterUpdate'], [
                    'url' => 'https://example.com/myAwesomeWebhook'
                ]],
                ['app\MyModel', 'afterInsert', [
                    'url' => 'https://example.com/MyAwesomeModelWebhook'
                ]],
                function($sender) {
                    // use closure as filter, return true if you want to send the webhook
                }
            ]
        ],
    ]
]
```
